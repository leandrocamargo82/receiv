<?php

namespace App\Views\Dividas;

use App\Controllers\TitulosController;
use App\Controllers\DevedoresController;

require('views/template/header.php');

$titulosAll = new TitulosController();
$devedoresAll = new DevedoresController();

$titulos = array();
$devedores = $devedoresAll->all();

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if (isset($_POST['id'])) {
        if (isset($_POST['delete'])) {
            $respostaDel = $titulosAll->delete($_POST['id']);
            $titulos = $titulosAll->all();
        } else {
            $titulos = $titulosAll->pesquisa($_POST['id']);
        }
    } elseif (isset($_POST['adiciona'])) {
        $resposta = $titulosAll->save($_POST);
        $titulos = $titulosAll->all();

    } else {
        http_response_code(404);
    }
} else {
    $titulos = $titulosAll->all();
}
?>

<div class="container-titulos">
    <?php
        if (isset($resposta)) {
            if ($resposta == 1) {
                echo '
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>OK!</strong> Título inserido com sucesso.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                ';
            } else {
                echo '
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Erro!</strong> Não foi possível criar novo Título.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            ';
            }
        }

        if (isset($respostaDel)) {
            if ($respostaDel == 1) {
                echo '
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>OK!</strong> Título excluído com sucesso.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                ';
            } else {
                echo '
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Erro!</strong> Não foi possível excluir o Título.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            ';
            }
        }
    ?>

    <div class="row mb-3">
        <div class="col-12 d-flex justify-content-end">
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modelId">
                <i class="fa fa-plus"> Novo Título</i>
            </button>
        </div>
    </div>

    <!-- Listagem -->
    <div class="table-titulos">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Devedor</th>
                    <th scope="col">Título</th>
                    <th scope="col">Valor</th>
                    <th scope="col">Vencimento</th>
                    <th scope="col">Status</th>
                    <th scope="col">Ações</th>
                </tr>
            </thead>
            <tbody>
            <?php
            if (!empty($titulos)):
                foreach ($titulos as $titulo):
                    ?>
                    <tr>
                        <th scope="row"><?= $titulo->id ?></th>
                        <td><?= $titulo->nome ?></td>
                        <td><?= $titulo->descricao_titulo ?></td>
                        <td><span> R$ <?= $titulo->valor ?></td>
                        <td><?= date('d/m/Y', strtotime($titulo->data_vencimento)) ?></td>
                        <td><?= ($titulo->data_vencimento < date("Y-m-d") ? 'Vencido' : 'A Vencer') ?></td>
                        <td>
                            <form action="/titulos" method="post">
                                <input type="hidden" name="id" value="<?= $titulo->id ?>">
                                <button type="submit" name="view" class="btn btn-warning" title="Detalhes"><i class="fa fa-eye"></i></button>
                            </form>
                        </td>
                    </tr>
                <?php
                endforeach;
                else:
                echo "<h5>Não há Títulos registrados no momento!!!</h5>";
            endif;
            ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Cadastro de título -->
<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <form action="/titulos" method="post">
        <div class="modal-dialog modal-lg" role="document">

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Novo Título</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Descrição Título</label>
                        <input type="text" class="form-control" name=":descricao_titulo" id="">
                    </div>
                    <div class="form-group">
                        <label for="">Valor do Título</label>
                        <input type="text" class="form-control" name=":valor" id="" onkeypress="$(this).mask('##0,00', {reverse : true})" required>
                    </div>
                    <div class="form-group">
                        <label for="">Vencimento do Título</label>
                        <input type="date" class="form-control" name=":data_vencimento" id="" required>
                    </div>
                    <div class="form-group">
                        <label for="">Devedor</label>
                        <select name=":id_devedor" id="" class="form-control selectpicker" data-live-search="true">
                            <option value="" selected disabled>Selecione um devedor</option>
                            <?php foreach ($devedores as $devedor): ?>
                                <option value="<?= $devedor->id ?>" ><?= $devedor->nome ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                    <button type="submit" name="adiciona" class="btn btn-success">Salvar</button>
                </div>
            </div>
        </div>
    </form>
</div>

<?php
$scripts = array(
    "https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"
);
require(BASE_PATH . '/views/template/footer.php');
?>
