<?php

namespace App\Views\Dividas;

use App\Controllers\TitulosController;
use App\Controllers\DevedoresController;

require('views/template/header.php');

$titulosAll = new TitulosController();
$devedoresAll = new DevedoresController();

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if (isset($_POST['id'])) {
        if (isset($_POST['atualiza'])) {
            $resposta = $titulosAll->update($_POST['id'], $_POST);
        }
        $devedores = $devedoresAll->all();
        $titulo = $titulosAll->pesquisa($_POST['id']);
    } else {
        http_response_code(404);
    }
}
?>

<div class="container-titulos">
    <?php
    if (isset($resposta)) {
        if ($resposta == 1) {
            echo '
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>OK!</strong> Título atualizado com sucesso.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                ';
        } else {
            echo '
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strongErro!</strong> Não foi possível atualizar o Título.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            ';
        }
    }
    ?>

    <div class="jumbotron mt-4 mt-md-2">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="invoice-title">
                        <h2><?= $titulo->descricao_titulo ?></h2>
                        <h3 class="pull-right">ID <?= $titulo->id ?></h3>
                        <div class="">
                            <button class="btn btn-warning text-white mr-3" data-toggle="modal" data-target="#modal-edit"><i class="far fa-edit"></i> Editar Título</button>
                            <button class="btn btn-danger" data-toggle="modal" data-target="#modal-delete"><i class="fas fa-file-alt"></i> Excluir Título</button>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <address>
                                <strong>Devedor</strong><br>
                                <?= $titulo->nome ?><br>
                                <?= $titulo->cpf_cnpj ?><br>
                            </address>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <address>
                                <strong>Endereço</strong><br>
                                <?= $titulo->endereco . ' - ' . $titulo->cidade . ', ' . $titulo->cep ?><br>
                            </address>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <address>
                                <strong>Título</strong><br>
                                Valor: R$ <?= $titulo->valor ?><br>
                                Data do Vencimento: <?= date('d/m/Y', strtotime($titulo->data_vencimento)) ?><br>
                            </address>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Edição de Título -->
<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <form action="/titulos" method="post">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edição do Título</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Descrição do Título</label>
                        <input type="text" class="form-control" value="<?= $titulo->descricao_titulo ?>" name=":descricao_titulo" id="">
                    </div>
                    <div class="form-group">
                        <label for="">Valor</label>
                        <input type="text" class="form-control" value="<?= $titulo->valor ?>" name=":valor" id="" required>
                    </div>
                    <div class="form-group">
                        <label for="">Vencimento</label>
                        <input type="date" class="form-control" value="<?= $titulo->data_vencimento ?>" onload='$(this).mask("00/00/0000")' name=":data_vencimento" id="" required>
                    </div>
                    <div class="form-group">
                        <label for="">Devedor</label>
                        <select name=":id_devedor" id="" class="form-control">
                            <?php foreach ($devedores as $devedor): ?>
                                <option value="<?= $devedor->id ?>" <?= ($devedor->id == $titulo->id_devedor ? 'selected' : '') ?>><?= $devedor->nome ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                    <input type="hidden" value="<?= $titulo->id ?>" name="id">
                    <button type="submit" name="atualiza" class="btn btn-success">Salvar</button>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Exclusão de Titulo -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmação</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Deseja excluir o Título <?= $titulo->descricao_titulo ?>?
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <form action="/titulos" method="post">
                    <input type="hidden" name="id" value="<?= $titulo->id ?>">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-success" name="delete">Continuar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
    require(BASE_PATH . '/views/template/footer.php');
?>

