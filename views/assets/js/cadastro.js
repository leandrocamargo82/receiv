$('#cep').change(event => {
    var cep = event.currentTarget.value;
    var xmlReq = new XMLHttpRequest();
    xmlReq.open('GET', `https://viacep.com.br/ws/${cep}/json/`);
    xmlReq.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            infos = JSON.parse(xmlReq.responseText);
            try {
                $('#cidade').val(infos.localidade);
                $('#endereco').val(infos.logradouro);
            } catch (erro) {
                console.error(erro)
            }
        }
    }
    xmlReq.send();
})
