<?php

namespace App\Views\Devedores;

use App\Controllers\DevedoresController;
use DateTime;

require(BASE_PATH . '/views/template/header.php');

$devedoresAll = new DevedoresController();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['nome'])) {
        $devedores = $devedoresAll->search($_POST['nome']);
    }elseif(isset($_POST['adiciona'])){
        $resposta = $devedoresAll->save($_POST);
        $devedores = $devedoresAll->all();
    }elseif(isset($_POST['delete'])){
        $respostaDel = $devedoresAll->delete($_POST['id']);
        $devedores = $devedoresAll->all();
    }
} else {
    $devedores = $devedoresAll->all();
}
?>

<div class="container-devedores">
    <?php
        // Mensagem de Retorno para Cadastro 
        if(isset($resposta)){
            if($resposta == 1){
                echo '
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>OK!</strong> Cadastro Realizado com sucesso.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                ';
            }else{
                echo '
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Erro!</strong> Não foi possível realizar cadastro.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            ';
            }
        }

        if(isset($respostaDel)){
            if($respostaDel == 1){
                echo '
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>OK!</strong> Devedor excluído com sucesso.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                ';
            }else{
                echo '
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Erro!</strong> Não foi possível excluir Devedor.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            ';
            }
        }
    ?>

    <div class="row mb-3">
        <div class="col-12 d-flex justify-content-end">
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modelId">
                <i class="fa fa-plus"> Novo Devedor</i>
            </button>
        </div>
    </div>

    <div class="row">
        <div class="container table-devedores">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nome</th>
                        <th scope="col">CPF/CNPJ</th>
                        <th scope="col">Data Nascimento</th>
                        <th scope="col">Logradouro</th>
                        <th scope="col">Cidade</th>
                        <th scope="col">CEP</th>
                        <th scope="col">Ações</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($devedores)):
                    foreach ($devedores as $devedor):
                        ?>
                        <tr>
                            <th scope="row"><?= $devedor->id ?></th>
                            <td><?= $devedor->nome ?></td>
                            <td><?= $devedor->cpf_cnpj ?></td>
                            <td><?= date('d/m/Y', strtotime($devedor->data_nascimento)) ?></td>
                            <td><?= $devedor->endereco ?></td>
                            <td><?= $devedor->cidade ?></td>
                            <td><?= $devedor->cep ?></td>
                            <td>
                                <form action="/devedores" method="post">
                                    <input type="hidden" name="id" value="<?= $devedor->id ?>">
                                    <button type="submit" name="view" class="btn btn-warning" title="Detalhes"><i class="fa fa-eye"></i></button>
                                </form>
                            </td>
                        </tr>
                    <?php
                    endforeach;
                else:
                    echo "<h5>Não há Devedores registrados no momento!!!</h5>";
                endif;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Cadastro de devedor -->
<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <form action="/devedores" method="post">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cadastro de Devedor</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">CPF/CNPJ</label>
                        <input type="text" class="form-control" name=":cpf_cnpj" id="" onkeypress="$(this).mask('000.000.000-00')" required>
                    </div>
                    <div class="form-group">
                        <label for="">Nome</label>
                        <input type="text" class="form-control" name=":nome" id="" required>
                    </div>
                    <div class="form-group">
                        <label for="">Data de Nascimento</label>
                        <input type="date" class="datepicker form-control" name=":data_nascimento" id="" required>
                    </div>
                    <div class="form-group">
                        <label for="">CEP</label>
                        <input type="text" class="form-control" name=":cep" onkeypress='$(this).mask("00000-000")' id="cep" required>
                    </div>
                    <div class="form-group">
                        <label for="">Endereço</label>
                        <input type="text" class="form-control" name=":endereco" id="endereco" required>
                    </div>
                    <div class="form-group">
                        <label for="">Cidade</label>
                        <input type="text" class="form-control" name=":cidade" id="cidade" required>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                    <button type="submit" name="adiciona" class="btn btn-success">Salvar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<?php
$scripts = array(
    "views/assets/js/cadastro.js",
    "https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"
);
require(BASE_PATH . '/views/template/footer.php');
?>