<?php
namespace App\Views\Devedores;

use App\Controllers\DevedoresController;
use App\Controllers\TitulosController;

require('views/template/header.php');

$devedoresAll = new DevedoresController();
$titulosAll = new TitulosController();

$devedor = null;
$titulos = array();

if($_SERVER['REQUEST_METHOD'] == "POST"){
    if(isset($_POST['id'])){
        if(isset($_POST['atualiza'])){
            $resposta = $devedoresAll->update($_POST['id'], $_POST);
        }
        $devedor = $devedoresAll->getByID($_POST['id']);
        $titulos = $titulosAll->getTitulosByDevedor($_POST['id']);
    }
    else{
        http_response_code(404);
    }
}
?>

<div class="container-devedores">
    <?php
        if(isset($resposta)){
            if($resposta == 1){
                echo '
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>OK!</strong> Atualização Realizado com sucesso.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                ';
            }else{
                echo '
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strongErro!</strong> Não foi possível atualizar cadastro.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            ';
            }
        }
    ?>

    <div class="row my-2 mb-md-3">
        <div class="container col-6">
            <div class="card card-info-devedor">
                <div class="card-body">
                    <div class="col-12 col-md-6">
                        <h5 class="card-title"><?= $devedor->nome ?></h5>
                        <div class="">
                            <button class="btn btn-warning text-white" data-toggle="modal" data-target="#modal-edit"><i class="far fa-edit"></i> Editar Devedor</button>
                            <button class="btn btn-danger ml-3" data-toggle="modal" data-target="#modal-delete"><i class="fas fa-user"></i> Excluir Devedor</button>
                        </div>
                        <hr>
                        <p class="card-text"><span>CPF:</span> <?=$devedor->cpf_cnpj ?></p>
                        <p class="card-text"><span>Endereço:</span> <?= $devedor->endereco . ',' . $devedor->cidade . '-' . $devedor->cep ?></p>
                        <p class="card-text"><span>Data de Nascimento:</span> <?= date('d/m/Y', strtotime($devedor->data_nascimento)) ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row table-titulos">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Devedor</th>
                <th scope="col">Título</th>
                <th scope="col">Valor</th>
                <th scope="col">Vencimento</th>
                <th scope="col">Status</th>
                <th scope="col">Ações</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if (!empty($titulos)):
                foreach ($titulos as $titulo):
                    ?>
                    <tr>
                        <th scope="row"><?= $titulo->id ?></th>
                        <td><?= $titulo->nome ?></td>
                        <td><?= $titulo->descricao_titulo ?></td>
                        <td><span> R$ <?= $titulo->valor ?></td>
                        <td><?= date('d/m/Y', strtotime($titulo->data_vencimento)) ?></td>
                        <td><?= ($titulo->data_vencimento < date("Y-m-d") ? 'vencida' : 'a vencer') ?></td>
                        <td>
                            <form action="/titulos" method="post">
                                <input type="hidden" name="id" value="<?= $titulo->id ?>">
                                <button type="submit" name="view" class="btn btn-warning" title="Detalhes"><i class="fa fa-eye"></i></button>
                            </form>
                        </td>
                    </tr>
                <?php
                endforeach;
            else:
                echo "<h5>Não há Títulos registrados para este Devedor no momento!!!</h5>";
            endif;
            ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Edição de devedor -->
<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <form action="/devedores" method="post">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edição de Devedores</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">CPF</label>
                        <input type="text" class="form-control" value="<?= $devedor->cpf_cnpj ?>" name=":cpf_cnpj" id="" onload='$(this).mask("000.000.000-00")' onkeypress='$(this).mask("000.000.000-00")' required>
                    </div>
                    <div class="form-group">
                        <label for="">Nome</label>
                        <input type="text" class="form-control" value="<?= $devedor->nome ?>" name=":nome" id="" required>
                    </div>
                    <div class="form-group">
                        <label for="">Data de Nascimento</label>
                        <input type="date" class="form-control" value="<?= $devedor->data_nascimento ?>" onload='$(this).mask("00/00/0000")' name=":data_nascimento" id="" required>
                    </div>
                    <div class="form-group">
                        <label for="">CEP</label>
                        <input type="text" class="form-control" value="<?= $devedor->cep ?>" name=":cep" onload='$(this).mask("00000-000")' id="cep" onkeypress='$(this).mask("00000-000")' id="cep" required>
                    </div>
                    <div class="form-group">
                        <label for="">Endereço</label>
                        <input type="text" class="form-control" value="<?= $devedor->endereco ?>" name=":endereco" id="endereco" required>
                    </div>
                    <div class="form-group">
                        <label for="">Cidade</label>
                        <input type="text" class="form-control" value="<?= $devedor->cidade ?>" name=":cidade" id="cidade" required>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                    <input type="hidden" value="<?=$devedor->id?>" name="id">
                    <button type="submit" name="atualiza" class="btn btn-success">Salvar</button>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Modal exclusão de devedor -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmação</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                Deseja excluir registro do Devedor <?= $devedor->nome ?>?
                <div class="alert alert-danger mt-2" role="alert">
                    <strong>Atenção</strong>, a remoção do registro implica na exclusão de todas as dívidas atreladas ao seu CPF.
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <form action="/devedores" method="post">
                    <input type="hidden" name="id" value="<?= $devedor->id ?>">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-success" name="delete">Excluir</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
$scripts = array(
    "views/assets/js/cadastro.js",
    "https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js",
);
require(BASE_PATH . '/views/template/footer.php');
?>
