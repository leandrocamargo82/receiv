<!doctype html>
<html lang="pt-BR">

<head>
    <title>Test DEV</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

    <!-- Font Awesome  -->
    <script src="https://kit.fontawesome.com/ca27e18b89.js" crossorigin="anonymous"></script>
    
    <!-- Custom CSS  -->
    <link rel="stylesheet" href="views/assets/css/style.css">
    <?php 
      if(isset($css)){
        foreach($css as $c){
          echo '<link rel="stylesheet" href="'. $c .'"';
        }
      }
    ?>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                </li>
            
                <li class="nav-item">
                    <a class="nav-link" href="/devedores">Devedores</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/titulos">Títulos</a>
                </li>
            </ul>
        </div>
    </nav>
