<?php

$css = array(
    "https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css",
);

include(BASE_PATH . '/views/template/header.php');

use App\Controllers\DashboardsController;

$dashboard = new DashboardsController();
$valor1 = round($dashboard->somaVencidos()->soma, 2);
$valor2 = round($dashboard->somaAVencer()->soma, 2);

?>

<div class="container-fluid">
    <div class="dash-container">
        <p id="valor1"><?= $valor1 ?></p>
        <p id="valor2"><?= $valor2 ?></p>
        <div class="card-body">
            <canvas id="chart-dash" ></canvas>
        </div>
    </div>
</div>

<?php
$scripts = array(
    "https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js",
    "views/assets/js/dash.js"
);
include(BASE_PATH . '/views/template/footer.php');
?>
