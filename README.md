Bom dia a todos, faço votos que estejam bem, se cuidando e gozando de saúde.

Primeiramente gostaria de agradecer pela opotunidade de participar do processo seletivo,

Desde já reconheço que não desenvolvi um bom projeto, mas, vamos às configurações:

Configuração do projeto:

Desenvolvido em ambiente apache (MAMP), alternativa ao XAMPP;

Requer alguns componentes instalados:
-composer;
-node;
-gulp;

Após o clone, importar o arquivo receiv.sql localizado na raiz;

Configurar o arquivo config.php, localizado na raiz, para acesso ao banco local (MySQL);

Acessar a pasta do projeto e rodar os comando via terminal:
composer install
npm install

Por fim, rodar o comando, também via terminal:

gulp connect-sync

Para iniciar o browser-sync