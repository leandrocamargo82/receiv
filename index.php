<?php

require('vendor/autoload.php');

$request = $_SERVER['REQUEST_URI'];

switch ($request) {
    case '/' :
        require 'views/index.php';
        break;
    case '' :
        require 'views/index.php';
        break;
    case '/devedores' :
        if($_SERVER['REQUEST_METHOD'] == "POST"){
            // Botão pesquisar navbar
            if(isset($_POST['pesquisa'])){
                require 'views/devedores/index.php';
            }
            // Modal Cadastro de Devedor
            elseif(isset($_POST['adiciona'])){
                require 'views/devedores/index.php';
            }
            // Ver dívidas e Edit de Devedor
            elseif(isset($_POST['view'])){
                require 'views/devedores/view.php';
            }
            // Deletar Devedor e suas respectivas Dívidas [Controlador]
            elseif(isset($_POST['delete'])){
                require 'views/devedores/index.php';
            }
            // Update de Devedor [Controlador]
            elseif(isset($_POST['atualiza'])){
                require 'views/devedores/view.php';
            }
            // Erro de Handler Request
            else{
                http_response_code(404);
                require 'views/404.php';
                break;
            }
        }else{
            // GET devedores => Return All()
            require 'views/devedores/index.php';
        }
        break;
    case '/titulos':
        if($_SERVER['REQUEST_METHOD'] == "POST"){
            // Adicionar Nova Dívida
            if(isset($_POST['adiciona'])){
                require 'views/titulos/index.php';
            }
            // View de única dívida, edit e delete
            elseif(isset($_POST['view'])){
                require 'views/titulos/view.php';
            }
            // Deletar Dívida [Controlador]
            elseif(isset($_POST['delete'])){
                require 'views/titulos/index.php';
            }
            // Update de Dívida [Controlador]
            elseif(isset($_POST['atualiza'])) {
                require 'views/titulos/view.php';
            }
            // Voltar
            elseif(isset($_POST['voltar'])){
                require 'views/titulos/index.php';
            }else{
                http_response_code(404);
                require 'views/404.php';
                break;
            }
        }else{
            require 'views/titulos/index.php';
        }
        break;
    case '/maiores':
        require 'views/dashboards/maiores.php';
        break;
    case '/vencimento':
        require 'views/dashboards/index.php';
        break;
    default:
        http_response_code(404);
        require 'views/404.php';
        break;
}
