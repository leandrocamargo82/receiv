<?php

namespace App\Controllers;

use App\Models\Titulo;

class TitulosController {

    public function all(){
        $titulos = new Titulo();

        return $titulos->index();
    }

    public function getByID($data){
        $titulos = new Titulo();
    }

    public function getTitulosByDevedor($id){
        $titulos = new Titulo();

        return $titulos->find(array(":id" => $id));
    }

    public function delete($data){
        $titulos = new Titulo();

        return $titulos->delete(array(":id" => $data));
    }

    public function update($id, $data) {
        $titulos = new Titulo();

        $data[':id'] = intval($id);
        $data[':descricao_titulo'] = strval($data[':descricao_titulo']);
        $data[':data_vencimento'] = implode('-', array_reverse(explode('/', $data[':data_vencimento'])));
        $data[':valor'] = implode('.', array_reverse(explode(',', floatval($data[':valor']))));
        $data[':id_devedor'] = intval($data[':id_devedor']);
        unset($data['atualiza']);
        unset($data['id']);

        return $titulos->update($data);
    }

    public function save($data){
        $titulos = new Titulo();

        $data[':data_vencimento'] = implode('-', array_reverse(explode('/', $data[':data_vencimento'])));
        $data[':valor'] = implode('.', array_reverse(explode(',', floatval($data[':valor']))));
        unset($data['adiciona']);

        return $titulos->create($data);
    }

    public function pesquisa($id){
        $devedores = new Titulo();

        return $devedores->pesquisa(array(":id" => $id));
    }
}
