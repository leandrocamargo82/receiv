<?php

namespace App\Controllers;

use App\Models\Devedor;

class DevedoresController {

    public function all(){
        $devedores = new Devedor();

        return $devedores->index();
    }

    public function getByID($data){
        $devedores = new Devedor();

        return $devedores->getByID(array(":id" => $data));
    }

    public function delete($data){
        $devedores = new Devedor();

        return $devedores->delete(array(":id" => $data));
    }

    public function update($id, $data){
        $devedores = new Devedor();

        $data[':id'] = intval($id);
        $data[':nome'] = strval($data[':nome']);
        $data[':cpf_cnpj'] = intval(preg_replace("/[^a-zA-Z0-9]/", "", $data[':cpf_cnpj']));
        $data[':data_nascimento'] = implode('-', array_reverse(explode('/', $data[':data_nascimento'])));
        $data[':endereco'] = strval($data[':endereco']);
        $data[':cidade'] = strval($data[':cidade']);
        $data[':cep'] = strval($data[':cep']);
        unset($data['atualiza']);
        unset($data['id']);

        return $devedores->update($id, $data);
    }

    public function save($data){
        $devedores = new Devedor();

        $data[':cpf_cnpj'] = intval(preg_replace("/[^a-zA-Z0-9]/", "", $data[':cpf_cnpj']));
        $data[':data_nascimento'] = implode('-', array_reverse(explode('/', $data[':data_nascimento'])));
        unset($data['adiciona']);

        return $devedores->create($data);
    }

    public function search($cpf_cnpj){
        $devedores = new Devedor();

        return $devedores->pesquisa(array(":cpf_cnpj" => '%'. $cpf_cnpj . '%'));
    }
}
