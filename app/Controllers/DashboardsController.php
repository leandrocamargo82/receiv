<?php

namespace App\Controllers;

use App\Models\Dashboard;

class DashboardsController {

    public function somaVencidos(){
        $dash =  new Dashboard();

        return $dash->vencidos();
    }

    public function somaAVencer(){
        $dash = new Dashboard();

        return $dash->aVencer();
    }
}
