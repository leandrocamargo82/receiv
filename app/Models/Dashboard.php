<?php

namespace App\Models;

use App\Database;
use PDOException;
use PDO;

class Dashboard
{
    public function vencidos(){
        $DB = new Database();
        $conn = $DB->connection();
        $query = "SELECT SUM(valor) as soma, AVG(valor) as media FROM titulos WHERE STR_TO_DATE(data_vencimento, '%Y-%m-%d') <= :hoje AND EXTRACT(YEAR FROM data_vencimento) = :ano_atual";
        $dates = array(
            ":hoje"      => date('Y-m-d'),
            ":ano_atual" => date('Y')
        );
        $stmt = $conn->prepare($query);
        try{
            $stmt->execute($dates);
            return $stmt->fetch(PDO::FETCH_OBJ);
        }catch(PDOException $e){
            echo $e->getMessage();
            return null;
        }
    }

    public function aVencer(){
        $DB = new Database();
        $conn = $DB->connection();
        $query = "SELECT SUM(valor) as soma, AVG(valor) as media FROM titulos WHERE STR_TO_DATE(data_vencimento, '%Y-%m-%d') >= :hoje AND EXTRACT(YEAR FROM data_vencimento) = :ano_atual";
        $dates = array(
            ":hoje"      => date('Y-m-d'),
            ":ano_atual" => date('Y')
        );
        $stmt = $conn->prepare($query);
        try{
            $stmt->execute($dates);
            return $stmt->fetch(PDO::FETCH_OBJ);
        }catch(PDOException $e){
            echo $e->getMessage();
            return null;
        }
    }
}
