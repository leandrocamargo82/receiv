<?php

namespace App\Models;

use App\Database;
use PDOException;
use PDO;

class Titulo{

    function index(){
        $DB = new Database();
        $conn = $DB->connection();
        $query = "SELECT titulos.*, devedores.nome as nome FROM titulos
                    INNER JOIN devedores ON titulos.id_devedor = devedores.id 
                    ORDER BY STR_TO_DATE(data_vencimento, '%Y-%m-%d') ASC";
        $stmt = $conn->prepare($query);

        try{
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }catch(PDOException $e){
            if(ENV == 'development'){
                echo $e->getMessage();
            }
            return null;
        }
    }

    public function pesquisa($nome){
        $DB = new Database();
        $conn = $DB->connection();
        $query = "SELECT titulos.*, devedores.nome as nome, devedores.cpf_cnpj, devedores.endereco, devedores.cidade, devedores.cep 
                    FROM titulos
                    INNER JOIN devedores ON titulos.id_devedor = devedores.id WHERE titulos.id = :id";
        $stmt = $conn->prepare($query);

        try{
            $stmt->execute($nome);

            return $stmt->fetch(PDO::FETCH_OBJ);
        }catch(PDOException $e){
            if(ENV == 'development'){
                echo $e->getMessage();
            }
            return null;
        }
    }

    public function find($id){
        $DB = new Database();
        $conn = $DB->connection();
        $query = "SELECT devedores.*, titulos.id, titulos.descricao_titulo, titulos.data_vencimento, titulos.valor 
                    FROM devedores
                    INNER JOIN titulos ON devedores.id = titulos.id_devedor WHERE devedores.id = :id";
        $stmt = $conn->prepare($query);

        try{
            $stmt->execute($id);
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }catch(PDOException $e){
            if(ENV == 'development'){
                echo $e->getMessage();
            }
            return null;
        }
    }

    public function update($data){
        $DB = new Database();
        $conn = $DB->connection();
        $query = "UPDATE titulos SET `id_devedor` = :id_devedor, `descricao_titulo` = :descricao_titulo, `data_vencimento` = :data_vencimento, `valor`= :valor WHERE id = :id";
        $stmt = $conn->prepare($query);

        try{
            $stmt->execute($data);
            return 1;
        }catch(PDOException $e){
            if(ENV == 'development'){
                echo $e->getMessage();
            }
            return 0;
        }
    }

    public function create($data){
        $DB = new Database();
        $conn = $DB->connection();
        $query = "INSERT INTO titulos (`id_devedor`, `descricao_titulo`, `data_vencimento`, `valor`) VALUES (:id_devedor, :descricao_titulo, :data_vencimento, :valor)";
        $stmt = $conn->prepare($query);

        try{
            $stmt->execute($data);
            return 1;
        }catch(PDOException $e){
            if(ENV == 'development'){
                echo $e->getMessage();
            }
            return 0;
        }
    }

    public function delete($id){
        $DB = new Database();
        $conn = $DB->connection();
        $query = "DELETE FROM titulos WHERE id = :id";
        $stmt = $conn->prepare($query);

        try{
            $stmt->execute($id);
            return 1;
        }catch(PDOException $e){
            if(ENV == 'development'){
                echo $e->getMessage();
            }
            return 0;
        }
    }
}