<?php
namespace App\Models;

use App\Database;
use PDOException;
use PDO;

class Devedor{

    public function index(){
        $DB = new Database();
        $conn = $DB->connection();
        $query = "SELECT * FROM devedores";
        $stmt = $conn->prepare($query);
        try{
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }catch(PDOException $e){
            if(ENV == 'development'){
                echo $e->getMessage();
            }
            return null;
        }
    }

    public function pesquisa($nome){
        $DB = new Database();
        $conn = $DB->connection();
        $query = "SELECT id, nome, cidade, cpf_cnpj, data_nascimento FROM devedores  WHERE `nome` LIKE :nome";
        $stmt = $conn->prepare($query);
        try{
            $stmt->execute($nome);
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }catch(PDOException $e){
            if(ENV == 'development'){
                echo $e->getMessage();
            }
            return null;
        }
    }

    public function getByID($id){
        $DB = new Database();
        $conn = $DB->connection();
        $query = "SELECT * FROM devedores WHERE `id` = :id";
        $stmt = $conn->prepare($query);
        try{
            $stmt->execute($id);
            return $stmt->fetch(PDO::FETCH_OBJ);
        }catch(PDOException $e){
            if(ENV == 'development'){
                echo $e->getMessage();
            }
            return null;
        }
    }

    public function create($data){
        $DB = new Database();
        $conn = $DB->connection();
        $query = "INSERT INTO devedores(`cpf_cnpj`, `nome`, `data_nascimento`, `cep`, `endereco`, `cidade`) VALUES (:cpf_cnpj, :nome, :data_nascimento, :cep, :endereco, :cidade) ";
        $stmt = $conn->prepare($query);

        try{
            $stmt->execute($data);
            return 1;
        }catch(PDOException $e){
            if(ENV == 'development'){
                echo $e->getMessage();
            }
            return 0;
        }
    }

    public function update($id, $data){
        $DB = new Database();
        $conn = $DB->connection();
        $query = "UPDATE devedores SET `cpf_cnpj` = :cpf_cnpj, `nome` = :nome, `data_nascimento` = :data_nascimento, `cep` = :cep, `endereco` = :endereco, `cidade` = :cidade WHERE id = :id";
        $stmt = $conn->prepare($query);
        try{
            $stmt->execute($data);
            return 1;
        }catch(PDOException $e){
            if(ENV == 'development'){
                echo $e->getMessage();
            }
            return 0;
        }
    }

    public function delete($id){
        $DB = new Database();
        $conn = $DB->connection();
        $query = "DELETE FROM devedores WHERE id = :id";
        $stmt = $conn->prepare($query);
        try{
            $stmt->execute($id);
            return 1;
        }catch(PDOException $e){
            if(ENV == 'development'){
                echo $e->getMessage();
            }
            return 0;
        }
    }
}