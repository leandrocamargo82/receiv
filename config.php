<?php

// Config file
define('BASE_PATH', dirname(__FILE__));

define('DB_HOST', '127.0.0.1');
define('DB_NAME', 'receiv');
define('DB_USER', 'root');
define('DB_PASS', 'root');
define('DB_PORT', '3306');

// development 
define("ENV", 'development');
//define("ENV", 'production');
ini_set('display_errors', true);
error_reporting(E_ALL);
date_default_timezone_set('America/Sao_Paulo');